# Rails Full-Stack Extension Pack

This extension pack packages some of the most popular and useful Ruby, Rails and Web Development extensions.

## Included Extensions

- [sorbet](https://marketplace.visualstudio.com/items?itemName=sorbet.sorbet-vscode-extension) - Ruby IDE features, powered by Sorbet.

- [Ruby LSP](https://marketplace.visualstudio.com/items?itemName=shopify.ruby-lsp)

- [Simple Sorbet Snippet](https://marketplace.visualstudio.com/items?itemName=MichalZaporski.simple-sorbet-snippets) - Simple snippets for Sorbet.

- [byesig](https://marketplace.visualstudio.com/items?itemName=itarato.byesig) - Hide Ruby Sorbet signatures

- [Rails Fast Nav](https://marketplace.visualstudio.com/items?itemName=jemmyw.rails-fast-nav) - Press `Alt + R` to navigate between `Controllers -> Tests -> Views` and `Models -> Tests`

  ![Rails Fast Nav](images/rails-fast-nav.gif)

- [Ruby Test Runner](https://marketplace.visualstudio.com/items?itemName=MateuszDrewniak.ruby-test-runner) - Run unit-test and rspec tests with one click!

  ![Ruby Test Runner](images/ruby-test-runner.gif)

- [Ruby Runner](https://marketplace.visualstudio.com/items?itemName=MateuszDrewniak.ruby-runner) - Run Ruby files with one click!

  ![Ruby Test Runner](images/ruby-runner.gif)

- [endwise](https://marketplace.visualstudio.com/items?itemName=kaiwood.endwise) - Wisely add "end" in Ruby.

- [erb](https://marketplace.visualstudio.com/items?itemName=CraigMaslowski.erb) - Syntax Highlighting for Ruby ERB files

- [YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) - YAML Language Support by Red Hat, with built-in Kubernetes syntax support

- [Procfile](https://marketplace.visualstudio.com/items?itemName=benspaulding.procfile) - Grammar & features for Procfiles

- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) - Autocompletion of file paths

- [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments) - Customizable coloring for comments

- [RBS Syntax Highlighting](https://marketplace.visualstudio.com/items?itemName=GracefulPotato.rbs-syntax) - Syntax highlighting for .rbs files

- [VSCode rdbg Ruby Debugger](https://marketplace.visualstudio.com/items?itemName=KoichiSasada.vscode-rdbg) - Debugging support for Ruby

- [Better Haml](https://marketplace.visualstudio.com/items?itemName=karunamurti.haml) - Better Haml syntax highlighting, auto close and suggestions

- [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight) - Highlight hex colors in the text editor

- [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) - Deploy your app through VSCode with Docker

- [NGINX Configuration](https://marketplace.visualstudio.com/items?itemName=william-voyek.vscode-nginx) - Syntax highlighting for nginx configuration files

- [Log File Highlighter](https://marketplace.visualstudio.com/items?itemName=emilast.LogFileHighlighter) - Make your log files easier to read with colors

- [Slim](https://marketplace.visualstudio.com/items?itemName=sianglim.slim) - Slim language support based on https://github.com/slim-template/ruby-slim.tmbundle

- [Sass](https://marketplace.visualstudio.com/items?itemName=syler.sass-indented) - Indented Sass syntax Highlighting, Autocomplete & Formatter

- [ERB Formatter/Beautify](https://marketplace.visualstudio.com/items?itemName=aliariff.vscode-erb-beautify) - Adds a VSCode formatter for .erb files

- [vscode-gemfile](https://marketplace.visualstudio.com/items?itemName=bung87.vscode-gemfile) - Adds links to online sites of gems on hover in Gemfile

- [gitignore](https://marketplace.visualstudio.com/items?itemName=codezombiech.gitignore) - Language support for .gitignore files. Lets you pull .gitignore files from the https://github.com/github/gitignore repository.

- [SVG Viewer](https://marketplace.visualstudio.com/items?itemName=cssho.vscode-svgviewer) - Let's you view SVG files in VSCode

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Integrates ESLint JavaScript into VS Code.

- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag) - Auto rename paired HTML/XML tag

- [Peacock](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock) - Subtly change the workspace color of your workspace. Ideal when you have multiple VS Code instances and you want to quickly identify which is which.

- [Dracula Dark+](https://marketplace.visualstudio.com/items?itemName=MateuszDrewniak.theme-dracula-dark-plus) - Yet another Dracula Official Theme fork, but with colors switched to resemble Dark+

- [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) - Support for dotenv file syntax

- [Duplicate action](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-duplicate) - Ability to duplicate files in VS Code

- [ERB Helper Tags](https://marketplace.visualstudio.com/items?itemName=rayhanw.erb-helpers) - A collection of ERB snippets

- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons) - Great Icons for Visual Studio Code

- [change-case](https://marketplace.visualstudio.com/items?itemName=wmaurer.change-case) - Quickly change the case (camelCase, CONSTANT_CASE, snake_case, etc) of the current selection or current word

- [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) - All you need to write Markdown (keyboard shortcuts, table of contents, auto preview and more)

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) extension

- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

- [Rainbow CSV](https://marketplace.visualstudio.com/items?itemName=mechatroner.rainbow-csv)

- [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)

- [RBS Snippets](https://marketplace.visualstudio.com/items?itemName=MateuszDrewniak.rbs-snippets)

- [VSCode Mogami](https://marketplace.visualstudio.com/items?itemName=ninoseki.vscode-mogami)

- [Better Align](https://marketplace.visualstudio.com/items?itemName=chouzz.vscode-better-align) - Easily align keys and values in hashes etc

**Enjoy!**
