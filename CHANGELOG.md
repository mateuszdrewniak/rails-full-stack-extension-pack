# Change Log

All notable changes to the "rails-full-stack-extension-pack" extension pack will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2] - 2024-07-15

### Added

- `JacobPfeifer.pfeifer-hurl`
- `MateuszDrewniak.hurl-runner`

## [0.2.1] - 2024-06-03

### Removed

- `LoranKloeze.ruby-rubocop-revived`
- `castwide.solargraph`


## [0.2.0] - 2024-06-03

### Added

- `ninoseki.vscode-mogami`
- `chouzz.vscode-better-align`
- `shopify.ruby-lsp`

### Removed

- `ninoseki.vscode-gem-lens`
- `wwm.better-align`
- `rebornix.ruby`
- `wingrunr21.vscode-ruby`
- `pavlitsky.yard`
- `MichalZaporski.simple-yard-snippets`


## [0.1.10] - 2024-05-14

### Added

- `MichalZaporski.simple-sorbet-snippets`

## [0.1.9] - 2024-05-08

### Added

- `sorbet.sorbet-vscode-extension`
- `itarato.byesig`

### Removed

- `castwide.solargraph`

## [0.1.8] - 2022-10-05

### Added

- `MateuszDrewniak.rbs-snippets`
- `GracefulPotato.rbs-syntax`

### Removed

- `soutaro.rbs-syntax`

## [0.1.7] - 2022-10-05

### Added

- `MichalZaporski.simple-yard-snippets`

## [0.1.6] - 2022-08-27

### Added

- `KoichiSasada.vscode-rdbg`

## [0.1.5] - 2022-08-14

### Added

- `christian-kohler.path-intellisense`
- `aaron-bond.better-comments`

## [0.1.4] - 2022-08-14

### Added

- `benspaulding.procfile`

### Changed

- `misogi.ruby-rubocop` replaced by `LoranKloeze.ruby-rubocop-revived`

## [0.1.3] - 2022-08-13

### Added

- soutaro.rbs-syntax
- naumovs.color-highlight
- ms-azuretools.vscode-docker
- emilast.LogFileHighlighter
- wwm.better-align
- william-voyek.vscode-nginx
- MateuszDrewniak.ruby-runner

### Removed

- aki77.rails-db-schema
- esbenp.prettier-vscode
- IBM.output-colorizer
- Janne252.fontawesome-autocomplete
- ms-vscode-remote.remote-wsl

## [0.1.2] - 2022-01-13

### Added

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) extension
- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
- [Rainbow CSV](https://marketplace.visualstudio.com/items?itemName=mechatroner.rainbow-csv)
- [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)

### Removed

- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

## [0.1.1] - 18.04.2021

### Changed

- **Dracula Refined** has been replaced by **Dracula Dark+**

## [0.1.0] - 17.04.2021

First version

### Added

Included extensions:

- "misogi.ruby-rubocop",
- "castwide.solargraph",
- "jemmyw.rails-fast-nav",
- "MateuszDrewniak.ruby-test-runner",
- "kaiwood.endwise",
- "aki77.rails-db-schema",
- "ninoseki.vscode-gem-lens",
- "rebornix.ruby",
- "wingrunr21.vscode-ruby",
- "CraigMaslowski.erb",
- "redhat.vscode-yaml",
- "karunamurti.haml",
- "sianglim.slim",
- "syler.sass-indented",
- "aliariff.vscode-erb-beautify",
- "bung87.vscode-gemfile",
- "pavlitsky.yard",
- "codezombiech.gitignore",
- "CoenraadS.bracket-pair-colorizer-2",
- "cssho.vscode-svgviewer",
- "dbaeumer.vscode-eslint",
- "esbenp.prettier-vscode",
- "formulahendry.auto-rename-tag",
- "IBM.output-colorizer",
- "Janne252.fontawesome-autocomplete",
- "johnpapa.vscode-peacock",
- "MateuszDrewniak.theme-dracula-dark-plus",
- "mikestead.dotenv",
- "mrmlnc.vscode-duplicate",
- "ms-vscode-remote.remote-wsl",
- "rayhanw.erb-helpers",
- "vscode-icons-team.vscode-icons",
- "wmaurer.change-case",
- "yzhang.markdown-all-in-one"
